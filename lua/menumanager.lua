local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.EnhancedHitmarkers = _G.EnhancedHitmarkers or {}
EnhancedHitmarkers._texture_reload_delay = 0.1
EnhancedHitmarkers._path = ModPath
EnhancedHitmarkers._data_path_old = SavePath .. 'enhanced_hitmarkers_options.txt'
EnhancedHitmarkers._data_path = SavePath .. 'EnhancedHitmarkers.json'
EnhancedHitmarkers._data = {}
EnhancedHitmarkers.tex_path = 'core/textures/'
EnhancedHitmarkers.tex_kill = 'editor_icons_df'
EnhancedHitmarkers.tex_hit = 'yellow'
EnhancedHitmarkers.override_path = EnhancedHitmarkers._path .. EnhancedHitmarkers.tex_path
EnhancedHitmarkers.settings = {}

function EnhancedHitmarkers:Reset(with_textures)
	self.settings.body = 'ff5500'
	self.settings.head = '57ff00'
	self.settings.crit = 'ff00ff'
	self.settings.blend_mode = 'normal'
	self.settings.shake = true

	local default_hit_texture = 'classic hit v2.texture'
	local default_kill_texture = 'Malo.texture'
	if with_textures then
		self:SetTextureHit(default_hit_texture)
		self:SetTextureKill(default_kill_texture)
	else
		self.settings.hit_texture = default_hit_texture
		self.settings.kill_texture = default_kill_texture
	end
end

function EnhancedHitmarkers:StoreColorSettings()
	self.settings.body = string.format('%02x%02x%02x', math.floor(self._data.BR * 100), math.floor(self._data.BG * 100),
		math.floor(self._data.BB * 100))
	self.settings.head = string.format('%02x%02x%02x', math.floor(self._data.HR * 100), math.floor(self._data.HG * 100),
		math.floor(self._data.HB * 100))
	self.settings.crit = string.format('%02x%02x%02x', math.floor(self._data.CR * 100), math.floor(self._data.CG * 100),
		math.floor(self._data.CB * 100))
end

function EnhancedHitmarkers:SettingsToData()
	self._data.BR = tonumber(string.sub(self.settings.body, 1, 2), 16) / 100
	self._data.BG = tonumber(string.sub(self.settings.body, 3, 4), 16) / 100
	self._data.BB = tonumber(string.sub(self.settings.body, 5, 6), 16) / 100

	self._data.HR = tonumber(string.sub(self.settings.head, 1, 2), 16) / 100
	self._data.HG = tonumber(string.sub(self.settings.head, 3, 4), 16) / 100
	self._data.HB = tonumber(string.sub(self.settings.head, 5, 6), 16) / 100

	self._data.CR = tonumber(string.sub(self.settings.crit, 1, 2), 16) / 100
	self._data.CG = tonumber(string.sub(self.settings.crit, 3, 4), 16) / 100
	self._data.CB = tonumber(string.sub(self.settings.crit, 5, 6), 16) / 100

	self._data.shake = self.settings.shake
	self._data.blend_mode = self.settings.blend_mode
end

-- returns true if changed
function EnhancedHitmarkers:SetTextureHit(filename, force)
	local source = self.override_path .. filename
	local target = self.override_path .. self.tex_hit .. '.texture'
	if not force and io.file_is_readable(target) and filename == self.settings.hit_texture then
		return false -- nothing changed
	end
	log('[Enhanced Hitmarkers] Setting hit texture: ' .. filename)
	local r = SystemFS:copy_file(source, target)
	log('[Enhanced Hitmarkers] --> ' .. (r and 'success' or 'failure'))
	if r then
		self.settings.hit_texture = filename
	end
	return r
end

-- returns true if changed
function EnhancedHitmarkers:SetTextureKill(filename, force)
	local source = self.override_path .. filename
	local target = self.override_path .. self.tex_kill .. '.texture'
	if not force and io.file_is_readable(target) and filename == self.settings.kill_texture then
		return false -- nothing changed
	end
	log('[Enhanced Hitmarkers] Setting kill texture: ' .. filename)
	local r = SystemFS:copy_file(source, target)
	log('[Enhanced Hitmarkers] --> ' .. (r and 'success' or 'failure'))
	if r then
		self.settings.kill_texture = filename
	end
	return r
end

function EnhancedHitmarkers:Migrate()
	if file.FileExists(self._data_path_old) and not file.FileExists(self._data_path) then
		SystemFS:rename_file(self._data_path_old, self._data_path)
	end
end

function EnhancedHitmarkers:Load()
	self:Reset(false)
	local file = io.open(self._data_path, 'r')
	if file then
		local settings = json.decode(file:read('*all'))
		file:close()
		if settings and type(settings) == "table" then
			for k, v in pairs(settings) do
				self.settings[k] = v
			end
		end
	end
	self:SettingsToData()
end

function EnhancedHitmarkers:Save()
	local file = io.open(self._data_path, 'w+')
	if file then
		self:StoreColorSettings()
		file:write(json.encode(self.settings))
		file:close()
	end

	if managers.hud then
		managers.hud:_create_hit_confirm()
	end
end

Hooks:Add('MenuManagerInitialize', 'MenuManagerInitialize_EnhancedHitmarkers', function(menu_manager)
	EnhancedHitmarkers:Migrate()
	EnhancedHitmarkers:Load()
end)
