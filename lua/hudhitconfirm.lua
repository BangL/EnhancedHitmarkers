local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local EnhancedHitmarkers = EnhancedHitmarkers

local eh_original_hudhitconfirm_init = HUDHitConfirm.init
function HUDHitConfirm:init(...)
	eh_original_hudhitconfirm_init(self, ...)
	self:reload_eh()
end

function HUDHitConfirm:reload_eh()
	local tex_kill = EnhancedHitmarkers.tex_path .. EnhancedHitmarkers.tex_kill
	local tex_hit = EnhancedHitmarkers.tex_path .. EnhancedHitmarkers.tex_hit
	local settings = EnhancedHitmarkers.settings
	local hms = {
		{ name = 'hit_body_confirm',  texture = tex_hit,  color = settings.body },
		{ name = 'hit_head_confirm',  texture = tex_hit,  color = settings.head },
		{ name = 'hit_crit_confirm',  texture = tex_hit,  color = settings.crit },
		{ name = 'kill_body_confirm', texture = tex_kill, color = settings.body },
		{ name = 'kill_head_confirm', texture = tex_kill, color = settings.head },
		{ name = 'kill_crit_confirm', texture = tex_kill, color = settings.crit }
	}
	local hp = self._hud_panel
	local blend_mode = settings.blend_mode

	-- prepare
	local eh_bitmaps = {}
	for i, hm in ipairs(hms) do
		if hp:child(hm.name) then
			hp:remove(hp:child(hm.name))
		end
		local bmp = hp:bitmap({
			valign = 'center',
			halign = 'center',
			visible = false,
			name = hm.name,
			texture = hm.texture,
			color = Color(hm.color),
			layer = 0,
			blend_mode = blend_mode
		})

		local w = bmp:texture_width()
		if w * 3 == bmp:texture_height() then
			bmp:set_texture_rect(0, math.mod(i - 1, 3) * w, w, w)
			bmp:set_height(w)
		end

		-- increase the size a bit
		bmp:set_size(w * 1.2, w * 1.2)

		bmp:set_center(hp:w() / 2, hp:h() / 2)
		eh_bitmaps[i] = bmp
	end
	-- commit
	self.eh_bitmaps = eh_bitmaps
end

function HUDHitConfirm:on_damage_confirmed(kill_confirmed, headshot, pos)
	local index = (kill_confirmed and 4 or 1) + (EnhancedHitmarkers.critshot and 2 or (headshot and 1 or 0))
	local hm = self.eh_bitmaps[index]

	hm:stop()
	if EnhancedHitmarkers.settings.shake then
		local rotation_angle = math.random(0, 8) - 4
		hm:rotate(rotation_angle)
	end
	hm:animate(callback(self, self, '_animate_show'), callback(self, self, 'show_done'), 0.25)
	if pos and managers.user:get_setting("hit_indicator") == 3 then
		hm:set_center(self:hud_pos_from_world(pos))
	else
		hm:set_center(self._hud_panel:w() / 2, self._hud_panel:h() / 2)
	end
end

function HUDHitConfirm:on_hit_confirmed(data, ...)
	local is_crit = data.is_crit or data.hit_type == HUDHitConfirm.HIT_WEAKPOINT
	if EnhancedHitmarkers.hooked then
		EnhancedHitmarkers.direct_hit = true
		EnhancedHitmarkers.critshot = is_crit
	else
		local i, priority = 1, 0
		if data.is_headshot then
			i = 2
			priority = priority + 1
		elseif is_crit then
			i = 3
			priority = priority + 2
		elseif data.hit_type == HUDHitConfirm.HIT_ARMOR then
			priority = priority - 2
		end
		if data.is_kill_shot then
			i = i + 3
			priority = priority + 4
		end
		local hit_con = self.eh_bitmaps[i]
		local is_minimalist_mode = managers.user:get_setting("hit_indicator") == HUDHitConfirm.MODE_MINIMAL
		local is_pellet = not is_minimalist_mode and data.hit_type == HUDHitConfirm.HIT_SPLINTER
		self:_queue_pop_hit(hit_con, data.pos, is_pellet, priority)
	end
end
