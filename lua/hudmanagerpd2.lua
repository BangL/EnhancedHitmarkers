local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function HUDManager:on_damage_confirmed(kill_confirmed, headshot, pos)
	if managers.user:get_setting('hit_indicator') > 1 then
		self._hud_hit_confirm:on_damage_confirmed(kill_confirmed, headshot, pos)
	end
end

function HUDManager:reload_eh()
	if self._hud_hit_confirm then
		self._hud_hit_confirm:reload_eh()
	end
end
