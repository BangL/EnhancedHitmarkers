
EnhancedHitmarkersMenu = EnhancedHitmarkersMenu or class(BLTMenu)

function EnhancedHitmarkersMenu:Init(root)
	self:Title({
		text = "eh_options_menu_title",
	})
	self:Label({
		text = nil,
		localize = false,
		h = 8,
	})
	self:SubTitle({text = "Body shot color", localize = false}) -- subtitle
	self:Slider({
		name = "eh_slider_colour_body_r",
		text = "eh_options_body_r_title",
		value = EnhancedHitmarkers._data.BR,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetRedBody"),
	})
	self:Slider({
		name = "eh_slider_colour_body_g",
		text = "eh_options_body_g_title",
		value = EnhancedHitmarkers._data.BG,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetGreenBody"),
	})
	self:Slider({
		name = "eh_slider_colour_body_b",
		text = "eh_options_body_b_title",
		value = EnhancedHitmarkers._data.BB,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetBlueBody"),
	})
	self:SubTitle({text = "Head shot color", localize = false}) -- subtitle
	self:Slider({
		name = "eh_slider_colour_head_r",
		text = "eh_options_head_r_title",
		value = EnhancedHitmarkers._data.HR,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetRedHead"),
	})
	self:Slider({
		name = "eh_slider_colour_head_g",
		text = "eh_options_head_g_title",
		value = EnhancedHitmarkers._data.HG,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetGreenHead"),
	})
	self:Slider({
		name = "eh_slider_colour_head_b",
		text = "eh_options_head_b_title",
		value = EnhancedHitmarkers._data.HB,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetBlueHead"),
	})
	self:SubTitle({text = "Crit color", localize = false}) -- subtitle
	self:Slider({
		name = "eh_slider_colour_crit_r",
		text = "eh_options_crit_r_title",
		value = EnhancedHitmarkers._data.CR,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetRedCrit"),
	})
	self:Slider({
		name = "eh_slider_colour_crit_g",
		text = "eh_options_crit_g_title",
		value = EnhancedHitmarkers._data.CG,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetGreenCrit"),
	})
	self:Slider({
		name = "eh_slider_colour_crit_b",
		text = "eh_options_crit_b_title",
		value = EnhancedHitmarkers._data.CB,
		min = 0,
		max = 2.55,
		callback = callback(self, self, "EnhancedHitmarkersSetBlueCrit"),
	})
	self:SubTitle({text = "Textures", localize = false}) -- subtitle
	self:MultiChoice({
		name = "eh_multi_texture_hit",
		text = "eh_options_texture_hit_title",
		callback = callback(self, self, "EnhancedHitmarkersSetTextureHit"),
		value = EnhancedHitmarkers.settings.hit_texture,
		items = {
			{text = "Almir", value = "almir.texture"},
			{text = "Aperture", value = "aperture.texture"},
			{text = "Checkbox H", value = "checkbox H.texture"},
			{text = "Checkbox K", value = "checkbox K.texture"},
			{text = "Classic crit", value = "classic crit.texture"},
			{text = "Classic hit v2", value = "classic hit v2.texture"},
			{text = "Classic hit", value = "classic hit.texture"},
			{text = "Horny", value = "horny.texture"},
			{text = "Hotline miami", value = "hotline miami.texture"},
			{text = "Loverkill", value = "loverkill.texture"},
			{text = "Malo", value = "Malo.texture"},
			{text = "MLG", value = "mlg.texture"},
			{text = "Nope", value = "nope.texture"},
			{text = "OVK", value = "ovk.texture"},
			{text = "Pattee", value = "pattee.texture"},
			{text = "PD2", value = "pd2.texture"},
			{text = "Phatcross", value = "phatcross.texture"},
			{text = "Pirate H", value = "pirate H.texture"},
			{text = "Pirate K", value = "pirate K.texture"},
			{text = "Pooverkill", value = "pooverkill.texture"},
			{text = "Radioactive", value = "radioactive.texture"},
			{text = "Smiley", value = "smiley.texture"},
			{text = "Star", value = "star.texture"},
			{text = "TdlQ", value = "TdlQ.texture"},
			{text = "Tongue and lip", value = "tongue and lip.texture"}
		}
	})
	self:MultiChoice({
		name = "eh_multi_texture_kill",
		text = "eh_options_texture_kill_title",
		callback = callback(self, self, "EnhancedHitmarkersSetTextureKill"),
		value = EnhancedHitmarkers.settings.kill_texture,
		items = {
			{text = "Almir", value = "almir.texture"},
			{text = "Aperture", value = "aperture.texture"},
			{text = "Checkbox H", value = "checkbox H.texture"},
			{text = "Checkbox K", value = "checkbox K.texture"},
			{text = "Classic crit", value = "classic crit.texture"},
			{text = "Classic hit v2", value = "classic hit v2.texture"},
			{text = "Classic hit", value = "classic hit.texture"},
			{text = "Horny", value = "horny.texture"},
			{text = "Hotline miami", value = "hotline miami.texture"},
			{text = "Loverkill", value = "loverkill.texture"},
			{text = "Malo", value = "Malo.texture"},
			{text = "MLG", value = "mlg.texture"},
			{text = "Nope", value = "nope.texture"},
			{text = "OVK", value = "ovk.texture"},
			{text = "Pattee", value = "pattee.texture"},
			{text = "PD2", value = "pd2.texture"},
			{text = "Phatcross", value = "phatcross.texture"},
			{text = "Pirate H", value = "pirate H.texture"},
			{text = "Pirate K", value = "pirate K.texture"},
			{text = "Pooverkill", value = "pooverkill.texture"},
			{text = "Radioactive", value = "radioactive.texture"},
			{text = "Smiley", value = "smiley.texture"},
			{text = "Star", value = "star.texture"},
			{text = "TdlQ", value = "TdlQ.texture"},
			{text = "Tongue and lip", value = "tongue and lip.texture"}
		}
	})
	self:SubTitle({text = "Rendering", localize = false}) -- subtitle
	self:MultiChoice({
		name = "eh_multi_set_blend_mode",
		text = "eh_options_set_blend_mode_title",
		callback = callback(self, self, "EnhancedHitmarkersSetBlendMode"),
		value = EnhancedHitmarkers.settings.blend_mode,
		items = {
			{text = "Normal", value = "normal"},
			{text = "Additive", value = "add"}
		}
	})
	self:Toggle({
		name = "eh_toggle_shake",
		text = "eh_options_shake_hitmarker_title",
		value = EnhancedHitmarkers.settings.shake,
		callback = callback(self, self, "EnhancedHitmarkersSetShake"),
	})
	self:LongRoundedButton2({
		name = "eh_options_reset",
		text = "eh_options_reset",
		localize = true,
		callback = callback(self, self, "Reset"),
		ignore_align = true,
		y = 832,
		x = 1472,
	})
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersHelp(value, item)
	EnhancedHitmarkers:DialogHelp()
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetRedBody(value, item)
	EnhancedHitmarkers._data.BR = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetGreenBody(value, item)
	EnhancedHitmarkers._data.BG = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetBlueBody(value, item)
	EnhancedHitmarkers._data.BB = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetRedHead(value, item)
	EnhancedHitmarkers._data.HR = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetGreenHead(value, item)
	EnhancedHitmarkers._data.HG = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetBlueHead(value, item)
	EnhancedHitmarkers._data.HB = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetRedCrit(value, item)
	EnhancedHitmarkers._data.CR = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetGreenCrit(value, item)
	EnhancedHitmarkers._data.CG = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetBlueCrit(value, item)
	EnhancedHitmarkers._data.CB = tonumber(value)
	EnhancedHitmarkers:StoreColorSettings()
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetTextureHit(value, item)
	if EnhancedHitmarkers:SetTextureHit(value.value) then
		self._needs_save = true
		self._texture_changed = true
	end
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetTextureKill(value, item)
	if EnhancedHitmarkers:SetTextureKill(value.value) then
		self._needs_save = true
		self._texture_changed = true
	end
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetShake(value, item)
	EnhancedHitmarkers.settings.shake = value
	self._needs_save = true
end

function EnhancedHitmarkersMenu:EnhancedHitmarkersSetBlendMode(value, item)
	EnhancedHitmarkers.settings.blend_mode = value.value
	self._needs_save = true
end

function EnhancedHitmarkersMenu:Reset(value, item)
	QuickMenu:new(
		managers.localization:text("eh_options_reset"),
		managers.localization:text("eh_options_reset_confirm"),
		{
			[1] = {
				text = managers.localization:text("dialog_no"),
				is_cancel_button = true,
			},
			[2] = {
				text = managers.localization:text("dialog_yes"),
				callback = function()
					EnhancedHitmarkers:Reset(true)
					EnhancedHitmarkers:SettingsToData()
					self._needs_save = true
					self._texture_changed = true
					self:ReloadMenu()
				end,
			},
		},
		true
	)
end

function EnhancedHitmarkersMenu:Close()
	if self._needs_save then
		EnhancedHitmarkers:Save()
	end
	if self._texture_changed and managers.hud and managers.hud.reload_eh then
		managers.hud:reload_eh()
	end
end

Hooks:Add("MenuComponentManagerInitialize", "EnhancedHitmarkersMenu.MenuComponentManagerInitialize", function(self)
	RaidMenuHelper:CreateMenu({
		name = "eh_options_menu",
		name_id = "eh_options_menu_title",
		inject_menu = "blt_options",
		class = EnhancedHitmarkersMenu
	})
end)
